//
//  Main.swift
//  GoVib
//
//  Created by Everton Carneiro on 11/04/2018.
//  Copyright © 2018 Everton. All rights reserved.
//

import UIKit
import CoreML
import Vision
import AVFoundation

class Main: UIViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var pickedImage: UIImageView!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var continueButtonOut: UIButton!
    @IBOutlet weak var changeImageButtonOut: RoundedButton!
    @IBOutlet weak var positivityLevel: UILabel!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var pickImageButtonOut: UIButton!
    
    let imagePicker = UIImagePickerController()
    var confidenceCounter = 1
    var confidencePercentage = 0
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImagepicker()
        hideObjects(true)
        sideMenu(menuButton: menuButton)
        
        continueButtonOut.alpha = 0.0
        changeImageButtonOut.alpha = 0.0
        pickImageButtonOut.alpha = 0.0
        pickedImage.layer.masksToBounds = true
        pickedImage.layer.cornerRadius = 10
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 2.0) {
            self.pickImageButtonOut.alpha = 1.0

        }
    }

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesBegan(touches, with: event)
//        guard let touch = touches.first else { return }
//
//        if touch.view == view {
//            present(imagePicker, animated: true, completion: nil)
//        }
//
//        UIView.animate(withDuration: 2.0) {
//            self.continueButtonOut.alpha = 0.0
//        }
//
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent 
    }
    
    func sideMenu(menuButton: UIBarButtonItem) {

        if revealViewController() != nil {

            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewRevealWidth = 225
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        }
    }

    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.02, target: self,   selector: (#selector(Main.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        confidenceCounter += 1
        if confidenceCounter < confidencePercentage {
            confidenceLabel.text = "\(confidenceCounter + 1)%"
        }else if confidenceCounter == confidencePercentage {
            confidenceCounter = 0
            timer.invalidate()
            animateButton()

        }
    }
    
    
    func hideObjects(_ bool: Bool) {
        pickedImage.isHidden = bool
        confidenceLabel.isHidden = bool
        positivityLevel.isHidden = bool
        line.isHidden = bool
    }
    
    func animateButton() {
        UIView.animate(withDuration: 2.0) {
            self.continueButtonOut.alpha = 1.0
            self.changeImageButtonOut.alpha = 1.0
        }
    }
    
    func setupImagepicker() {
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
    }
    
    
    func detect(image: CIImage) {
        
        if #available(iOS 11.0, *) {
            guard let model = try? VNCoreMLModel(for: VisualSentimentCNN().model) else {
                fatalError("Erro ao carregar coreml")/*Colocar alerta de erro aqui*/
            }
            
            let request = VNCoreMLRequest(model: model) { (request, error) in
                guard let results = request.results as? [VNClassificationObservation] else {
                    fatalError("Model fails to process image")
                }
                print(results)
                print(results[0].confidence)
                
                let first = results.first?.identifier
                var confidencePositive = VNConfidence()
                
                if first == "Positive" {
                    if let confidence = results.first?.confidence {
                        confidencePositive = confidence
                    }
                }else {
                    if let confidence = results.last?.confidence {
                        confidencePositive = confidence
                    }
                    
                }
                self.confidencePercentage = Int(confidencePositive*100 + 1)
                self.runTimer()

                
            }
            let handler = VNImageRequestHandler(ciImage: image)
            
            do {
                try handler.perform([request])
                
            }catch {
                print(error)
            }
            
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @IBAction func continueButton(_ sender: Any) {
    }
    
    
    @IBAction func changeImageButtonTapped(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
        UIView.animate(withDuration: 2.0) {
            self.continueButtonOut.alpha = 0.0
            self.changeImageButtonOut.alpha = 0.0
        }
    }
    
    @IBAction func pickImageButtonTapped(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
        UIView.animate(withDuration: 2.0) {
            self.continueButtonOut.alpha = 0.0
            self.changeImageButtonOut.alpha = 0.0
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! FilterViewController
        destination.pickedImage = pickedImage.image!
        destination.percentage = confidencePercentage
    }
    

}

extension Main: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
        pickedImage.image = image
        hideObjects(false)

        
        guard let ciimage = CIImage(image: image) else {
            fatalError("Não pôde converter image para ciimage")/*Colocar alerta de erro aqui*/
        }
        imagePicker.dismiss(animated: true) {
            //self.animateButton()
        }
        detect(image: ciimage)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true) {
            if let _ = self.pickedImage.image {
                self.animateButton()

            }
        }
    }
    
    
}









