//
//  FilterViewController.swift
//  GoVib
//
//  Created by Everton Carneiro on 21/04/2018.
//  Copyright © 2018 Everton. All rights reserved.
//

import UIKit
import CropViewController
import CoreImage


class FilterViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var sticker: UIImageView!
    @IBOutlet weak var filterNameLabel: UILabel!
    
    var pickedImage = UIImage()
    var croppedImage = UIImage()
    var percentage = Int()
    var isCropped = false
    var filterCounter = 0
    var colorCounter = 1
    static var photoList = [Photo]()
    
    //MARK: Overriden functions
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = pickedImage
        imageView.isUserInteractionEnabled = true
        percentageLabel.text = String(percentage)
        imageView.addSubview(sticker)
        imageView.addSubview(percentageLabel)
        filterNameLabel.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    func applyFilter(_ name: FilterName) {
        
        var image = UIImage()
        var cgImage: CGImage?
        
        if isCropped {
            image = croppedImage
            cgImage = image.cgImage
            
        } else {
            image = pickedImage
            cgImage = image.cgImage
        }

        let imgScale = image.scale
        let imgOrientation = image.imageOrientation
        
        let openGLContext = EAGLContext(api: .openGLES2)
        let context = CIContext(eaglContext: openGLContext!)
        
        
        let coreImage = CIImage(cgImage: cgImage!)
        
        
        let filter = CIFilter(name: name.rawValue)
        filter?.setValue(coreImage, forKey: kCIInputImageKey)
        if name == .CISepiaTone{
            filter?.setValue(0.7, forKey: kCIInputIntensityKey)
        }
        
        if let output = filter?.value(forKey: kCIOutputImageKey) as? CIImage {
            let cgImgResult = context.createCGImage(output, from: output.extent)
            let result = UIImage(cgImage: cgImgResult!, scale: imgScale, orientation: imgOrientation)
            imageView?.image = result
        }else {
            print("image filtering failed")
        }
 
    }
    
    //MARK: Buttons
    @IBAction func shareButtonTapped(_ sender: Any) {
        let imageToShare = [getEditedImageFromContext()]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
        FilterViewController.photoList.append(Photo(image: getEditedImageFromContext(), positivity: percentage))
    }
    

    @IBAction func cropButtonTapped(_ sender: Any) {
        presentCropViewController()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismissController()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func getEditedImageFromContext() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        imageView.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

    func dismissController() {
        DispatchQueue.main.async() {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: Gestures

    @IBAction func stickerTapped(_ sender: UITapGestureRecognizer) {
        
        sticker.tintColor = changeStickerColor(colorCounter)
        percentageLabel.textColor = changeStickerColor(colorCounter)
        colorCounter += 1
        if colorCounter > 4 {
            colorCounter = 0
        }
        
    }
    
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        animateFilterTransition()
        filterCounter += 1
        if filterCounter == 10 {
            filterCounter = 0
        }
        filterPicker(filterCounter)
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        animateFilterTransition()
        filterCounter -= 1
        if filterCounter == -1 {
            filterCounter = 9
        }
        filterPicker(filterCounter)
       
    }
    
    func changeStickerColor(_ colorNumber: Int) -> UIColor? {
        switch colorNumber {
        case 0: return UIColor.StickerColor.white
        case 1: return UIColor.StickerColor.red
        case 2: return UIColor.StickerColor.yellow
        case 3: return UIColor.StickerColor.purple
        case 4: return UIColor.StickerColor.black
        default:
            break
        }
        return nil
    }
    
    func filterPicker(_ filterNumber: Int) {
        switch filterCounter {
        case 0:
            filterNameLabel.text = "Original"
            if isCropped {
                imageView.image = croppedImage
            }else {
                imageView.image = pickedImage
            }
        case 1:
            applyFilter(.CIPhotoEffectTransfer)
            filterNameLabel.text = "Transfer"
        case 2:
            applyFilter(.CIPhotoEffectProcess)
            filterNameLabel.text = "Process"
        case 3:
            applyFilter(.CIPhotoEffectInstant)
            filterNameLabel.text = "Instant"
        case 4:
            applyFilter(.CIPhotoEffectFade)
            filterNameLabel.text = "Fade"
        case 5:
            applyFilter(.CIPhotoEffectChrome)
            filterNameLabel.text = "Chrome"
        case 6:
            applyFilter(.CISepiaTone)
            filterNameLabel.text = "Sepia"
        case 7:
            applyFilter(.CIPhotoEffectNoir)
            filterNameLabel.text = "Noir"
        case 8:
            applyFilter(.CIPhotoEffectTonal)
            filterNameLabel.text = "Tonal"
        case 9:
            applyFilter(.CIPhotoEffectMono)
            filterNameLabel.text = "Mono"
        default:
            break
        }
    }
    
    func animateFilterTransition() {
        UIView.animate(withDuration: 0.2, animations: {
            self.imageView.alpha = 0.7
            self.filterNameLabel.alpha = 1
        }) { (finished) in
            UIView.animate(withDuration: 0.2, animations: {
                self.imageView.alpha = 1
            })
            UIView.animate(withDuration: 1, animations: {
                self.filterNameLabel.alpha = 0
            })
        }
    }
 
}

extension FilterViewController: CropViewControllerDelegate {
    
    func presentCropViewController() {
        let image: UIImage = pickedImage
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        croppedImage = image
        isCropped = true
        imageView.image = croppedImage
        
        cropViewController.dismiss(animated: true, completion: nil) 
    }
    
    
}
