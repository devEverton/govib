//
//  FilterNames.swift
//  GoVib
//
//  Created by Everton Carneiro on 06/05/2018.
//  Copyright © 2018 Everton. All rights reserved.
//

import Foundation

enum FilterName: String {
    
    case CIPhotoEffectTransfer = "CIPhotoEffectTransfer"
    case CIPhotoEffectProcess = "CIPhotoEffectProcess"
    case CIPhotoEffectInstant = "CIPhotoEffectInstant"
    case CIPhotoEffectFade = "CIPhotoEffectFade"
    case CIPhotoEffectChrome = "CIPhotoEffectChrome"
    case CISepiaTone = "CISepiaTone"
    case CIPhotoEffectNoir = "CIPhotoEffectNoir"
    case CIPhotoEffectTonal = "CIPhotoEffectTonal"
    case CIPhotoEffectMono = "CIPhotoEffectMono"
    
}
