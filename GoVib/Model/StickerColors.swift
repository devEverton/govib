//
//  StickerColors.swift
//  GoVib
//
//  Created by Everton Carneiro on 06/05/2018.
//  Copyright © 2018 Everton. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct StickerColor {
        static var red: UIColor { return UIColor(red:0.78, green:0.37, blue:0.39, alpha:1.0)}
        static var yellow: UIColor { return UIColor(red:0.96, green:0.88, blue:0.72, alpha:1.0)}
        static var black: UIColor { return UIColor(red:0.20, green:0.21, blue:0.27, alpha:1.0)}
        static var purple: UIColor { return UIColor(red:0.52, green:0.34, blue:0.49, alpha:1.0)}
        static var white: UIColor { return UIColor.white}
    }
}


