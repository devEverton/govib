//
//  Photo.swift
//  GoVib
//
//  Created by Everton Carneiro on 14/05/2018.
//  Copyright © 2018 Everton. All rights reserved.
//

import Foundation
import UIKit

struct Photo {
    
    var image: UIImage
    var positivity: Int
    
}
